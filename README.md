Step to use this Ansible Roles :

 1. Clone this Ansible Playbook :
	 
	git clone https://gitlab.com/dhanielluis/ansible-sonarqube-rhel.git
	  
 2. this ansible roles use vagrant to test, the Vagrantfile and playbook in sonarqube/tests/ :
    
        $ cd sonarqube/tests/
        $ vagrant up
        
        
 3. The Vagrant file configured with IP (192.168.68.10), after ansible playbook executed SonarQube can be directly access to 192.168.68.10:9000

	http://192.168.68.10:900
    
